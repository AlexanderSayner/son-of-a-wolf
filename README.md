# Son of a Wolf - Powerwolf

Tonight alone you're riding  
Onto the other side  
Your might untold and waiting  
Boy, you'll be the final light

Your mother fought the demons  
Your brother saw the light  
And now we pray the unborn  
You and I can't stay alive  


Son of a wolf, when the fight is calling  
Son of a wolf, and the night has come  
Son of a wolf, can you hear me calling, haleluja  


Take you another liar  
And never do the sign  
When you were harmed and taken  
Boy, you'll see the night alight  


Forever to the brave men  
When times were hard and wild  
You are the new messiah  
Born a wolf of human kind  


Son of a wolf, when the fight is calling  
Son of a wolf, and the night has come  
Son of a wolf, can you hear me calling, haleluja  

Son of a wolf, can you see him dying  
Son of a wolf, when the night is dark  
Son of a wolf, can you hear them calling, haleluja  


Agnus dei in tempestis, ignarus et animus  
Sanctus iesu in tormentis, Romulus in misera  
Agnus dei in tempestis, ignarus et animus  
Sanctus iesu in tormentis, Romulus et Lupercalia  


Son of a wolf, when the fight is calling  
Son of a wolf, when the night is dark  
Son of a wolf, can you hear me calling, haleluja  

Son of a wolf, can you see him dying  
Son of a wolf, when the night is dark  
Son of a wolf, can you hear them calling, haleluja

Son of a wolf  
